var loginButton = document.getElementById('login');
loginButton.addEventListener('click', login);

window.onload = function() {
  firebase.auth().signOut();
}

function login() {
  var username = document.getElementById('username').value;
  var password = document.getElementById('password').value;

  firebase.auth().signInWithEmailAndPassword(username, password).then(function(user) {
    // check if the user is staff
    firebase.database().ref('staff/' + user.uid).once('value').then(function(snapshot) {
      if (snapshot.val() != null) {
        window.location = 'staff.html';
      }
      // check if the user is student
      else {
        firebase.database().ref('projects/' + user.uid).once('value').then(function(snapshot) {
          if (snapshot.val() != null) {
            window.location = 'student.html';
          }
        });
      }
    });

  }).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    document.getElementById('error').textContent = error.message;
    document.getElementById('error').style = 'display: inline;';
  });

  // if (username == 'student' && password == 'student') {
  //   window.location = 'https://rjmacfadyen.bitbucket.io/store/forms/student.html';
  // }
  // else if (username == 'staff' && password == 'staff') {
  //   window.location = 'https://rjmacfadyen.bitbucket.io/store/forms/staff.html';
  // }
  // else {
  //   document.getElementById('error').style = 'display: inline;';
  // }
}
