window.onload = function() {
  document.getElementById('studentform').style = 'display: none;'
  document.getElementById('projectload').style = 'display: inline;'

  //userID = firebase.auth().currentUser.uid;
}

firebase.auth().onAuthStateChanged(function(user) {
  console.log('auth changed');
  if (user) {
    userID = user.uid
    loadProject(userID);
  } else {
    window.location.href = "login.html";
  }
});
