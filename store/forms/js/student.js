var userID = '';
var userName = '';

var tags = [];
var images = [];

var tagBox = document.getElementById('tag');
tagBox.addEventListener('input', parseTag);

var addTagButton = document.getElementById('addtag');
addTagButton.addEventListener('click', addTag);

// var addImageButton = document.getElementById('addimage');
// addImageButton.addEventListener('change', addImageDialog);

var addImageInput = document.getElementById('imageinput');
addImageInput.addEventListener('change', addImage);

var studentForm = document.getElementById('studentform');
studentForm.addEventListener('submit', writeProject);

var submitButton = document.getElementById('saveproject');
submitButton.addEventListener('click', writeProject);

var logOutButton = document.getElementById('logout');
logOutButton.addEventListener('click', logOut);



// auth methods
function logOut() {
  firebase.auth().signOut().then(function() {
    console.log('signed out');
  }).catch(function(error) {
    window.alert('An error occured: ' + error.message);
  });
}

// form methods
function parseTag() {
  var tag = tagBox.value;
  if (tag == '') {
    addTagButton.style = 'display: none;'
  }
  else {
    addTagButton.style = 'display: initial;'
    if (tag.slice(-1) == ' ' || tag.slice(-1) == ',' || tag.slice(-1) == ';') {
      addTag();
    }
  }
  console.log('parsetag');
}

function addTag() {
  var tag = tagBox.value.trimChars(' ,;');
  tagBox.value = '';
  if (tag != '') {
    if (tags.indexOf(tag) == -1) {
      // add the tag to the list
      if (tags.length < 10) {
        tags.push(tag);
        // add a label to the tag list
        var tagLabel = document.createElement('li');
        tagLabel.appendChild(document.createTextNode(tag + ' '));

        // create tag delete button
        var tagDeleteButton = document.createElement('button');
        tagDeleteButton.type = 'button';
        tagDeleteButton.className = 'deltag';
        tagDeleteButton.appendChild(document.createTextNode('\u00d7'));
        tagDeleteButton.addEventListener('click', delTag);

        tagLabel.appendChild(tagDeleteButton);

        document.getElementById('taglist').appendChild(tagLabel);
      }
    }
  }
  tagBox.focus();

  if (tagBox.value == '') {
    addTagButton.style = 'display: none;'
  }
  else {
    addTagButton.style = 'display: initial;'
  }
}

function delTag(event) {

  // select the tag from the label
  var tagLabel = event.srcElement.parentNode;
  var tag = tagLabel.firstChild.textContent.trim();

  // find the index of the tag in the list
  var index = tags.indexOf(tag);

  console.log(index);

  // remove the tag and tag label
  if (index != -1) {
    tags.splice(index, 1);
    tagLabel.parentNode.removeChild(tagLabel);
  }
}

function addImageDialog() {
  addImageInput.click();
}

function addImage() {

  // list of rejected files for error message
  var rejectedFiles = '';

  // get file list
  var files = addImageInput.files;
  if (files.length > 0) {
    for (var i = 0; i < files.length; i++) {
      // get each file
      var file = files.item(i);

      // check file type
      if (file.type == 'image/jpeg' || file.type == 'image/png') {
        if (file.size <= 2097152) { // 5MB
          var reader = new FileReader();

          reader.onload = (function() {return function(e) {
            var image = e.target.result;
            if (images.indexOf(image) == -1 && images.length < 4) {
              images.push(image);

              var imageThumb = document.createElement('img');
              imageThumb.src = image;
              imageThumb.height = 128;

              // add a label to the image list
              var imageLabel = document.createElement('li');
              imageLabel.appendChild(imageThumb);
              imageLabel.appendChild(document.createTextNode(' '));

              // create image delete button
              var imageDeleteButton = document.createElement('button');
              imageDeleteButton.type = 'button';
              imageDeleteButton.className = 'deltag';
              imageDeleteButton.appendChild(document.createTextNode('\u00d7'));
              imageDeleteButton.addEventListener('click', delImage);

              imageLabel.appendChild(imageDeleteButton);

              document.getElementById('imagelist').appendChild(imageLabel);
            }
          };})();
          reader.readAsDataURL(file);
        }
        else {
          // add file to rejected error message
          rejectedFiles += file.name + ", "
        }
      }
      else {
        // add file to rejected error message
        rejectedFiles += file.name + ", "
      }
    }
  }

  // show error messages
  var imageError = document.getElementById('imageerror');

  if (rejectedFiles != '') {
    imageError.innerHTML = 'Files were too large or wrong type: ' + rejectedFiles;
    imageError.style = 'display: inline;';
  }
  else {
    imageerror.style = '';
  }

  addImageInput.value = null;
}

function delImage(event) {
  // select the image from the label
  var imageLabel = event.srcElement.parentNode;
  var image = imageLabel.firstChild.src;

  // find the index of the image in the list
  var index = images.indexOf(image);

  // remove the image and image label
  if (index != -1) {
    images.splice(index, 1);
    imageLabel.parentNode.removeChild(imageLabel);
    console.log('removed');
  }
}

function loadProject(studentID) {
  firebase.database().ref('projects/' + studentID).once('value').then(function(snapshot) {
    if (snapshot.val() != null) {
      userID = studentID;
      userName = snapshot.val().studentName;
      document.getElementById('studentname').value = snapshot.val().studentName;
      document.getElementById('title').value = snapshot.val().projectName;

      if (snapshot.val().projectTags != null && snapshot.val().projectTags != "") {
        tags = snapshot.val().projectTags;
      }
      else {
        tags = [];
      }

      document.getElementById('notes').value = snapshot.val().projectDesc;

      if (snapshot.val().projectImages != null && snapshot.val().projectImages != "") {
        images = snapshot.val().projectImages;
      }
      else {
        images = [];
      }
      console.log('loaded');
    }
    else {
      window.alert('user does not exist');
    }

    studentForm.style = 'display: initial;'
    document.getElementById('projectload').style = 'display: none;'
    loadTagsAndImages();
  });
}

function loadProjectData(project) {
  userID = project.studentID;
  userName = project.studentName;
  document.getElementById('studentname').value = userName;
  document.getElementById('title').value = project.title;
  tags = project.tags;
  document.getElementById('notes').value = project.notes;
  images = project.images;

  loadTagsAndImages();
}

function loadTagsAndImages() {
  document.getElementById('taglist').innerHTML = '';
  if (tags != null) {
    for (var i = 0; i < tags.length; i++) {
      var tag = tags[i]

      // add a label to the tag list
      var tagLabel = document.createElement('li');
      tagLabel.appendChild(document.createTextNode(tag + ' '));

      // create tag delete button
      var tagDeleteButton = document.createElement('button');
      tagDeleteButton.type = 'button';
      tagDeleteButton.className = 'deltag';
      tagDeleteButton.appendChild(document.createTextNode('\u00d7'));
      tagDeleteButton.addEventListener('click', delTag);

      tagLabel.appendChild(tagDeleteButton);

      document.getElementById('taglist').appendChild(tagLabel);

      addTagButton.style = 'display: none;'
    }
  }

  document.getElementById('imagelist').innerHTML = '';
  if (images != null) {
    for (var i = 0; i < images.length; i++) {

        var image = images[i]

        var imageThumb = document.createElement('img');
        imageThumb.src = image;
        imageThumb.height = 128;

        // add a label to the image list
        var imageLabel = document.createElement('li');
        imageLabel.appendChild(imageThumb);

        // create image delete button
        var imageDeleteButton = document.createElement('button');
        imageDeleteButton.type = 'button';
        imageDeleteButton.className = 'deltag';
        imageDeleteButton.appendChild(document.createTextNode('\u00d7'));
        imageDeleteButton.addEventListener('click', delImage);

        imageLabel.appendChild(imageDeleteButton);

        document.getElementById('imagelist').appendChild(imageLabel);
    }
  }
}

function saveProjectData(studentID, studentName, sessionID) {
  var project = new Project(
    studentID,
    studentName,
    sessionID,
    document.getElementById('title').value + '',
    tags,
    document.getElementById('notes').value + '',
    images
  )

  return project;
}

function writeProject() {
  var project = saveProjectData(userID, userName, '');
  writeProjectData(project.studentID, project.studentName, project.sessionID, project.title, project.tags, project.notes, project.images)
  window.alert('Your project has been updated');
}
