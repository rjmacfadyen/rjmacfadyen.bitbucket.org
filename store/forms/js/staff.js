var projects = [];
var sessions = [];

var sessionIndex = 0;

var manager = 0; // 0 = projects, 1 = sessions

var projectIndex = null;

var moveLeftButton = document.getElementById('moveleft');
moveLeftButton.addEventListener('click', moveLeft);

var moveRightButton = document.getElementById('moveright');
moveRightButton.addEventListener('click', moveRight);

var sessionSelect = document.getElementById('session');
sessionSelect.addEventListener('change', refreshLists);

var saveProjectButton = document.getElementById('saveproject');
saveProjectButton.addEventListener('click', saveProject);

var unsortedList = document.getElementById('unsorted');
unsortedList.addEventListener('change', loadProjectFromUnsorted);

var sortedList = document.getElementById('sorted')
sortedList.addEventListener('change', loadProjectFromSorted);

var sessionManagerList = document.getElementById('currentsessions');
sessionManagerList.addEventListener('change', refreshSessionManager);

var editSessionsButton = document.getElementById('showsessionmanager');
editSessionsButton.addEventListener('click', showSessionManager);

var closeSessionsButton = document.getElementById('hidesessionmanager');
closeSessionsButton.addEventListener('click', hideSessionManager);

var newSessionButton = document.getElementById('newsession');
newSessionButton.addEventListener('click', newSessionManager);

var deleteSessionButton = document.getElementById('deletesession');
deleteSessionButton.addEventListener('click', deleteSessionManager);

var manageUsersButton = document.getElementById('manageusers');
manageUsersButton.addEventListener('click', manageUsers);


// preload sample projects for demo, should be replaced with server load
window.onload = function() {
  //loadSessions();
  //loadProjects();

  sessionID = '0';
  //refreshSessions();
  //refreshLists();
  refreshProject();
  refreshSessionManager();

  console.log('loaded');
}

firebase.auth().onAuthStateChanged(function(user) {
  console.log('auth changed');
  if (!user) {
    window.location.href = "login.html";
  }
});

function manageUsers() {
  window.location = 'users.html';
}

function refreshSessions() {
  //var sessionList = document.getElementById('session');
  sessionSelect.innerHTML = '';
  sessionManagerList.innerHTML = '';

  for (var i = 0; i < sessions.length; i++) {
    var session = sessions[i];

    var listItem = document.createElement('option');
    //console.log(session.sessionID);
    var str = session.location + ', ' + session.startTime + ' - ' + session.endTime;
    listItem.appendChild(document.createTextNode(str));
    listItem.value = session.sessionID;

    sessionSelect.appendChild(listItem);

    var listItem2 = listItem.cloneNode(true);
    sessionManagerList.appendChild(listItem2);
  }
}

function refreshLists() {
  sessionIndex = document.getElementById('session').value;
  //console.log(sessionIndex);

  unsortedList.innerHTML = '';
  sortedList.innerHTML = '';

  for (var i = 0; i < projects.length; i++) {
    if (projects[i].sessionID == '') {
      // add to unsorted list
      var project = projects[i];

      var listItem = document.createElement('option');
      //console.log(project.studentID);
      var str = project.studentName + ' - ' + ((project.title == '') ? ' (not submitted)' : project.title);
      listItem.appendChild(document.createTextNode(str));
      listItem.value = project.studentID;

      unsortedList.appendChild(listItem);
    }
    else if (projects[i].sessionID == sessionIndex) {
      // add to sorted list
      var project = projects[i];

      var listItem = document.createElement('option');
      //console.log(project.studentID);
      var str = project.studentName + ' - ' + project.title;
      listItem.appendChild(document.createTextNode(str));
      listItem.value = project.studentID;

      sortedList.appendChild(listItem);
    }
  }


  projectIndex = null;
  refreshProject();
}

function refreshProject() {
  document.getElementById('studentform').style = 'display: none;';
  if (projectIndex == null) {
    document.getElementById('projectload').style = 'display: none;';
    document.getElementById('projectnote').style = 'display: block;';
  }
  else {
    document.getElementById('projectload').style = 'display: block;';
    document.getElementById('projectnote').style = 'display: none;';
  }
}

function refreshSessionManager() {
  if (manager == 0) {
    document.getElementById('manager').style = 'display: initial';
    document.getElementById('sessionmanager').style = 'display: none';
  }
  else if (manager == 1) {
    document.getElementById('manager').style = 'display: none';
    document.getElementById('sessionmanager').style = 'display: initial';
  }
}

function showSessionManager() {
  manager = 1;
  document.getElementById('manager').style = 'display: none';
  document.getElementById('sessionmanager').style = 'display: initial';
  refreshSessionManager();
}

function hideSessionManager() {
  manager = 0;
  document.getElementById('manager').style = 'display: initial';
  document.getElementById('sessionmanager').style = 'display: none';
  //loadProjects();
}

function newSessionManager() {
  var location = document.getElementById('location').value;
  var startHour = document.getElementById('starthour').value;
  var startMinute = document.getElementById('startminute').value;
  var endHour = document.getElementById('endhour').value;
  var endMinute = document.getElementById('endminute').value;

  if (location != '' && isNumber(startHour) && isNumber(startMinute) && isNumber(endHour) && isNumber(endMinute)) {
    if (startHour >= 0 && startHour < 60 && endHour >= 0 && endHour < 60 && startMinute >= 0 && startMinute < 60 && endMinute >= 0 && endMinute < 60) {
      if ((startHour * 60 + startMinute) < (endHour * 60 + endMinute)) {
        newSession(location, pad(startHour, '0', 2) + ':' + pad(startMinute, '0', 2), pad(endHour, '0', 2) + ':' + pad(endMinute, '0', 2));
        window.alert('Session added');

        document.getElementById('location').value = '';
        document.getElementById('starthour').value = '';
        document.getElementById('startminute').value = '';
        document.getElementById('endhour').value = '';
        document.getElementById('endminute').value = '';

        refreshSessions;
      }
      else {
        window.alert('End time is before start time');
      }
    }
    else {
      window.alert('Times are out of range');
    }
  }
  else {
    window.alert('Fields are empty or time is not a number');
  }
}

function deleteSessionManager() {
  if (sessionManagerList.value != '') {
    if (window.confirm('Are you sure?\nProjects allocated to this session will be unallocated')) {
      deleteSession(sessionManagerList.value);
    }
  }
}

function loadProjectFromUnsorted() {
  if (getSelectedItems(unsortedList).length == 1) {
    loadProjectByID(unsortedList.value);
  }
  else if (getSelectedItems(sortedList).length != 1) {
    projectIndex = null;
  }

    refreshProject();
}

function loadProjectFromSorted() {
  if (getSelectedItems(sortedList).length == 1) {
    loadProjectByID(sortedList.value);
  }
  else if (getSelectedItems(unsortedList).length != 1) {
    projectIndex = null;
  }

    refreshProject();
}

function loadProjectByID(id) {
  var project = getProjectByID(id);
  if (project != null) {
    document.getElementById('projectload').style = 'display: inline;';
    loadProject(id);

  }
  else {
    console.log('project ' + id + ' does not exist, load failed');
  }
}

function saveProject() {
  var project = projects[projectIndex];

  if (project != null) {
    var newProject = saveProjectData(project.studentID, project.studentName, project.sessionID);
    console.log(newProject);
    writeProject();
    projects[projectIndex] = newProject;
  }
  else {
    console.log('project does not exist, save failed');
  }
}

function moveLeft() {

  var selected = getSelectedItems(sortedList);

  for (var i = 0; i < selected.length; i++) {
    setProjectSession(selected[i].value, '');
  }
}

function moveRight() {

  var selected = getSelectedItems(unsortedList);

  for (var i = 0; i < selected.length; i++) {
    setProjectSession(selected[i].value, sessionIndex);
  }
}

function getProjectByID(id) {
  for (var i = 0; i < projects.length; i++) {
    if (projects[i].studentID == id) {
      projectIndex = i;
      return projects[i];
    }
  }
  projectIndex = null;
  return null;
}

function getSessionByID(id) {
  for (var i = 0; i < sessions.length; i++) {
    if (sessions[i].sessionID == id) {
      return sessions[i];
    }
  }
  return null;
}

function getSelectedItems(select) {
  return Array.prototype.filter.apply(
    select.options, [
      function(o) {
        return o.selected;
      }
    ]
  )
}

function newSession(l, s, e) {
  firebase.database().ref('sessions/').once('value').then(function(snapshot) {
    if (snapshot.val() != null) {
      var sessionsList = snapshot.val();
      var newSession = new Session(sessionsList.length, l, s, e);
      sessionsList.push({
        location: l,
        startTime: s,
        endTime: e
      });
      firebase.database().ref('sessions').set(sessionsList);
      //loadSessions();
      //refreshSessions();
    }
  });
}

function deleteSession(id) {
  firebase.database().ref('sessions/').once('value').then(function(snapshot) {
    if (snapshot.val() != null) {
      var sessionsList = snapshot.val();
      if (id >= 0 && id < sessionsList.length) {
        sessionsList.splice(id, 1);

        projects = [];
        firebase.database().ref('projects/').once('value').then(function(snapshot) {
          if (snapshot.val() != null) {
            var projectsList = snapshot.val();
            snapshot.forEach(function(snapshot) {
              var p = new Project(snapshot.key, snapshot.val().studentName, snapshot.val().showcaseSession, snapshot.val().projectName, snapshot.val().projectTags, snapshot.val().projectDesc, snapshot.val().projectImages);

              if (p.sessionID == id) {p.sessionID = ""}
              else if (p.sessionID > id) {p.sessionID -= 1};

              if (p.tags == null) {p.tags = []};
              if (p.images == null) {p.images = []};

              writeProjectData(p.studentID, p.studentName, p.sessionID, p.title, p.tags, p.notes, p.images);
            });
            //loadSessions();
            //refreshSessions();
            //loadProjects();
          }
        });
      }
      firebase.database().ref('sessions').set(sessionsList);
    }
  });
}



// realtime methods to replace one-time methods
var projectsRef = firebase.database().ref('projects');
projectsRef.on('value', function(snapshot) {
  console.log('rtLoadProjects');
  rtLoadProjects(snapshot);
})

function rtLoadProjects(snapshot) {
  document.getElementById('listsload').style = 'display: initial;';

  projects = [];
  snapshot.forEach(function(snapshot) {
    var p = new Project(snapshot.key, snapshot.val().studentName, snapshot.val().showcaseSession, snapshot.val().projectName, snapshot.val().projectTags, snapshot.val().projectDesc, snapshot.val().projectImages);
    if (p.tags == null || p.tags == "") {p.tags = []};
    if (p.images == null || p.images == "") {p.images = []};
    projects.push(p);
  })

  document.getElementById('listsload').style = 'display: none;';
  refreshLists();
}

var sessionsRef = firebase.database().ref('sessions');
sessionsRef.on('value', function(snapshot) {
  console.log('rtLoadSessions');
  rtLoadSessions(snapshot);
})

function rtLoadSessions(snapshot) {
  document.getElementById('listsload').style = 'display: initial;';

  sessions = [];
  snapshot.forEach(function(snapshot) {
    var s = new Session(snapshot.key, snapshot.val().location, snapshot.val().startTime, snapshot.val().endTime);
    sessions.push(s);
  })

  document.getElementById('listsload').style = 'display: none;';
  refreshSessions();
  refreshLists();
}

// obsolete methods

// function loadProjects() {
//   console.log('loadProjects');
//   document.getElementById('listsload').style = 'display: initial;';
//   firebase.database().ref('projects/').once('value').then(function(snapshot) {
//     if (snapshot.val() != null) {
//       projects = [];
//       var projectsList = snapshot.val();
//
//       snapshot.forEach(function(snapshot) {
//         var p = new Project(snapshot.key, snapshot.val().studentName, snapshot.val().showcaseSession, snapshot.val().projectName, snapshot.val().projectTags, snapshot.val().projectDesc, snapshot.val().projectImages);
//         if (p.tags == null) {p.tags = []};
//         if (p.images == null) {p.images = []};
//         projects.push(p);
//       })
//
//       refreshLists();
//       document.getElementById('listsload').style = 'display: none;';
//     }
//   })
// }

// function loadSessions() {
//   console.log('loadSessions');
//   firebase.database().ref('sessions/').once('value').then(function(snapshot) {
//     if (snapshot.val() != null) {
//       sessions = [];
//       var sessionsList = snapshot.val();
//
//       for (var i = 0; i < sessionsList.length; i++) {
//         var s = new Session(i, sessionsList[i].location, sessionsList[i].startTime, sessionsList[i].endTime);
//         sessions.push(s);
//       }
//       refreshSessions();
//     }
//   });
// }
