function Project(id, n) {
  this.studentID = id;
  this.studentName = n;
  this.sessionID = '';
  this.title = '';
  this.tags = [];
  this.notes ='';
  this.images = [];
}

function Project(id, sn, s, t, tag, n, i) {
  this.studentID = id;
  this.studentName = sn;
  this.sessionID = s;
  this.title = t;
  this.tags = tag;
  this.notes = n;
  this.images = i;
}

function Session(id, l, s, e) {
  this.sessionID = id;
  this.location = l;
  this.startTime = s;
  this.endTime = e;
}

var database = firebase.database();

// function getProjectData(studentID) {
//   // return firebase.database().ref('projects/' + studentID).once('value').then(function(snapshot) {
//   // return snapshot.val();
//     // var name = snapshot.val().studentName;
//     // return new Project(studentID, name)
//   // });
//
//   var f = firebase.database().ref('projects/' + studentID).once('value').then(function(snapshot) {
//     console.log(snapshot.val().studentName);
//     return null;
//   });
//
//   console.log(f);
//   console.log('after');
// }

// function loadProject(studentID) {
//   var project = getProjectData(studentID).__proto__;
//   return project;
// }

function newProject(studentID, studentName) {
  firebase.database().ref('projects/' + studentID).set({
    studentName: studentName,
    showcaseSession: '',
    projectName: '',
    projectTags: '',
    projectDesc: '',
    projectImages: ''
  })
}

function removeProject(studentID) {
  firebase.database().ref('projects/' + studentID).remove();
}

function writeProjectData(studentID, name, sessionID, title, tags, notes, images) {
  firebase.database().ref('projects/' + studentID).set({
    studentName: name,
    showcaseSession: sessionID,
    projectName: title,
    projectTags: tags,
    projectDesc: notes,
    projectImages: images
  })
}

function setProjectSession(studentID, sessionID) {
  firebase.database().ref('projects/' + studentID + '/showcaseSession').set(sessionID);
}

function newSession(location, startTime, endTime) {
  // firebase.database().ref('sessions/')
}

function deleteSession(sessionID) {

}

function isNumber(n) {
  return !isNaN(parseInt(n));
}

function pad(s, c, n) {
  if (n < s.length) {
    return s.substring(s.length - n, s.length);
  }
  else {
    var z = '';
    for (var i = 0; i < n - s.length; i++) {
      z += c.substring(0,1);
    }
    return z + s;
  }
}

String.prototype.trimChars = function(chars) {
  var l = 0;
  var r = this.length - 1;
  while(chars.indexOf(this[l]) >= 0 && l < r) l++;
  while(chars.indexOf(this[r]) >= 0 && r >= l) r--;
  return this.substring(l, r + 1);
};
