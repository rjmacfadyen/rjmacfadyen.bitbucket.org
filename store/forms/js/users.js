var projects = [];

var usersList = document.getElementById('userslist');
// usersList.addEventListener('change', refreshList);

var newUserButton = document.getElementById('newuser');
newUserButton.addEventListener('click', newUser);

var deleteUserButton = document.getElementById('deleteuser');
deleteUserButton.addEventListener('click', deleteUser);

var manageProjectsButton = document.getElementById('manageprojects');
manageProjectsButton.addEventListener('click', manageProjects);
//
// var deleteAllButton = document.getElementById('deleteall');
// deleteAllButton.addEventListener('click', deleteAll);

var secondaryApp = firebase.initializeApp(config, "Secondary");

function manageProjects() {
  window.location = 'staff.html';
}

function newUser() {
    var email = document.getElementById('email').value;
    var name = document.getElementById('name').value;

    addStudent(email, name);

    document.getElementById('email').value = '';
    document.getElementById('name').value = '';

    refreshList();
}

firebase.auth().onAuthStateChanged(function(user) {
  console.log('auth changed');
  if (!user) {
    window.location.href = "login.html";
  }
});

function addStudent(email, name) {
  secondaryApp.auth().createUserWithEmailAndPassword(email, 'password').then(function(user) {
    newProject(user.uid, name);
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    window.alert(errorMessage);
  });
  secondaryApp.auth().signOut();
}

function deleteUser() {
  if (usersList.value != '') {
    if (window.confirm('Are you sure?\nThis user\'s project will be permanently deleted\nThe user\'s account will remain until deleted from the Firebase console')) {
      removeProject(usersList.value);
    }
  }
}

// function deleteAll() {
//   if (usersList.value != '') {
//     if (window.confirm('Are you sure?\nThis user\'s project will be permanently deleted\nThe user\'s account will remain until deleted from the Firebase console')) {
//       removeProject(projectsList.value);
//     }
//   }
// }
//
// function resetPassword() {
//   var secondaryApp = firebase.initializeApp(config, "Secondary");
//   secondaryApp.auth().createUserWithEmailAndPassword(email, 'password').then(function(user) {
//     newProject(user.uid, name);
//   }).catch(function(error) {
//     // Handle Errors here.
//     var errorCode = error.code;
//     var errorMessage = error.message;
//     window.alert(errorMessage);
//   });
//   secondaryApp.auth().signOut();
// }

var projectsRef = firebase.database().ref('projects');
projectsRef.on('value', function(snapshot) {
  console.log('rtLoadProjects');
  rtLoadProjects(snapshot);
})

function rtLoadProjects(snapshot) {
  projects = [];
  snapshot.forEach(function(snapshot) {
    var p = new Project(snapshot.key, snapshot.val().studentName, snapshot.val().showcaseSession, snapshot.val().projectName, snapshot.val().projectTags, snapshot.val().projectDesc, snapshot.val().projectImages);
    if (p.tags == null || p.tags == "") {p.tags = []};
    if (p.images == null || p.images == "") {p.images = []};
    projects.push(p);
  })
  refreshList();
}

function refreshList() {
  usersList.innerHTML = '';

  for (var i = 0; i < projects.length; i++) {
      var project = projects[i];

      var listItem = document.createElement('option');
      //console.log(project.studentID);
      var str = project.studentName + ((project.title == '') ? ' (not submitted)' : (' (' + project.title + ')'));
      listItem.appendChild(document.createTextNode(str));
      listItem.value = project.studentID;

      usersList.appendChild(listItem);
  }
}
