BasicGame.MainMenu = function (game) {

this.groundTiles;

this.scoreText;
this.titleText;

this.spaceKey;

};

BasicGame.MainMenu.prototype = {

	create: function () {

        console.log("Creating menu state!");

				// background
				groundTiles = this.add.tileSprite(0, 0, this.game.width, this.game.height, 'background');
		    groundTiles.fixedToCamera = true;

				// text
				scoreText = this.add.text(this.game.width / 2, 2, '', {fontSize: '24px', fontWeight: 'normal', fill: '#fff'});

				try { // score will not exist on first load so catch error
					scoreText.text = 'last score ' + score;
				}	catch(err) {

				}

				scoreText.x -= scoreText.width / 2;

				scoreText.fixedToCamera = true;

				titleText = this.add.text(this.game.width / 2, this.game.height / 2,'press space to start',{fontSize: '24px', fontWeight: 'normal', fill: '#fff'});
				titleText.x -= titleText.width / 2;
				titleText.y -= titleText.height / 2;

				titleText.fixedToCamera = true;

				// keys
				spaceKey = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

		// Add some buttons
	},

	update: function () {

		//	Do some nice funky main menu effect here
		groundTiles.tilePosition.x -= 1;
		groundTiles.tilePosition.y -= 0.5;

		if (spaceKey.isDown) {
			this.state.start('Game');
		}

	},

	resize: function (width, height) {

		//	If the game container is resized this function will be called automatically.
		//	You can use it to align sprites that should be fixed in place and other responsive display things.

	}

};
