BasicGame.Game = function(game) {

  //	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

  this.game; //	a reference to the currently running game
  this.add; //	used to add sprites, text, groups, etc
  this.camera; //	a reference to the game camera
  this.cache; //	the game cache
  this.input; //	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
  this.load; //	for preloading assets
  this.math; //	lots of useful common math operations
  this.sound; //	the sound manager - add a sound, play one, set-up markers, etc
  this.stage; //	the game stage
  this.time; //	the clock
  this.tweens; //  the tween manager
  this.state; //	the state manager
  this.world; //	the game world
  this.particles; //	the particle manager
  this.physics; //	the physics manager
  this.rnd; //	the repeatable random number generator



  //	You can use any of these from any function within this State.
  //	But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.
  this.player;
  this.bullets;

  this.enemies;
  this.enemybullets;

  this.groundTiles;

  this.upKey;
  this.downKey;
  this.leftKey;
  this.rightKey;
  this.mouse;

  this.overheat;
  this.overheated;
  this.shotCooldown;
  this.spawnCooldown;

  this.score;

  this.scoretext;
  this.lives;
  this.bars;

  this.debugGraphics;

};

BasicGame.Game.prototype = {

  create: function() {

    console.log("Creating game state!");

    this.world.setBounds(-480, -480, 960, 960);

    this.physics.startSystem(Phaser.Physics.ARCADE);

    this.stage.backgroundColor = '#002040';

    // background
    groundTiles = this.add.tileSprite(0, 0, this.game.width, this.game.height, 'background');
    groundTiles.fixedToCamera = true;

    // player
    player = this.add.sprite(0, 0, 'player');
    this.physics.arcade.enable(player);
    player.enableBody = true;
    player.body.setSize(16,16);
    player.body.offset.x = -2;
    player.body.collideWorldBounds = true;

    player.health = 3;

    player.anchor.x = 0.4;
    player.anchor.y = 0.5;

    this.game.camera.follow(player, Phaser.Camera.FOLLOW_LOCKON);

    // bullets
    bullets = this.add.group();
    this.physics.arcade.enable(bullets);
    bullets.enableBody = true;

    // enemies
    enemies = this.add.group();
    enemies.enableBody = true;

    // enemy bullets
    enemybullets = this.add.group();
    enemybullets.enableBody = true;

    // score
    score = 0;

    // display
    scoretext = this.add.text(2,0,'0',{fontSize: '24px', fontWeight: 'normal', fill: '#fff'});
    scoretext.fixedToCamera = true;

    lives = this.add.sprite(this.game.width - 74, 2, 'lives');
    lives.fixedToCamera = true;
    lives.animations.frame = 0;

    bars = this.add.graphics(0,0);
    bars.fixedToCamera = true;
    bars.visible = true;
    bars.lineWidth = 2;

    debugGraphics = this.add.graphics(0,0);

    // cooldowns
    overheat = 0;
    overheated = false;
    shotCooldown = 0;
    spawnCooldown = 0;

    // controls
    upKey = this.input.keyboard.addKey(Phaser.KeyCode.W);
    downKey = this.input.keyboard.addKey(Phaser.KeyCode.S);
    leftKey = this.input.keyboard.addKey(Phaser.KeyCode.A);
    rightKey = this.input.keyboard.addKey(Phaser.KeyCode.D);

    mouseLeft = this.input.mousePointer.leftButton;
    mousePosition = this.input.position;

    math = this.math;

  },

  update: function() {

    // move player with WASD
    if (upKey.isDown) {
      player.body.y--;
    }

    if (downKey.isDown) {
      player.body.y++;
    }

    if (leftKey.isDown) {
      player.body.x--;
    }

    if (rightKey.isDown) {
      player.body.x++;
    }

    groundTiles.tilePosition.x = -this.camera.x;
    groundTiles.tilePosition.y = -this.camera.y;

    // point player towards mouse
    angle = math.angleBetween(player.x - this.camera.x, player.y - this.camera.y, mousePosition.x, mousePosition.y);

    player.rotation = angle;

    // if mouse is down, shoot
    if (mouseLeft.isDown && shotCooldown <= 0 && !overheated) {
      bullet = bullets.create(player.x + Math.cos(player.rotation) * 10, player.y + Math.sin(player.rotation) * 10, 'player-bullet');
      bullet.angle = player.angle;
      bullet.body.velocity.x = Math.cos(player.rotation) * 100;
      bullet.body.velocity.y = Math.sin(player.rotation) * 100;
      bullet.pivot.x = 2;
      bullet.pivot.y = 1;

      overheat += 25;
      shotCooldown = 10;
    }

    // spawn enemies
    if (spawnCooldown <= 0) {
      enemy = enemies.create(-480 + Math.random() * 960, -480 + Math.random() * 960, 'enemy');
      enemy.body.setSize(16,16);
      enemy.body.offset.x = -2;
      enemy.anchor.x = 0.4;
      enemy.anchor.y = 0.5;

      enemy.shotCooldown = 91;

      spawnCooldown = 150;
    }

    // move enemies
    enemies.forEachAlive(function(enemy) {

      // point towards player
      enemy.rotation = math.angleBetween(enemy.x, enemy.y, player.x, player.y);
      enemy.body.velocity.x = Math.cos(enemy.rotation) * 40;
      enemy.body.velocity.y = Math.sin(enemy.rotation) * 40;

      if (enemy.shotCooldown <= 0) {
        enemybullet = enemybullets.create(enemy.x + Math.cos(enemy.rotation) * 10, enemy.y + Math.sin(enemy.rotation) * 10, 'enemy-bullet')
        enemybullet.angle = enemy.angle;
        enemybullet.body.velocity.x = Math.cos(enemy.rotation) * 100;
        enemybullet.body.velocity.y = Math.sin(enemy.rotation) * 100;
        enemybullet.pivot.x = 2;
        enemybullet.pivot.y = 1;

        enemy.shotCooldown = 91;
      }

      enemy.shotCooldown--;

    }, this);

    // shoot enemies
    this.physics.arcade.overlap(bullets, enemies, function(bullet, enemy) {
      enemy.kill();

      bullet.kill();

      score++;
    }, null, this);

    // get shot by enemies
    this.physics.arcade.overlap(enemybullets, player, function(player, bullet) {
      player.health--;
      lives.animations.frame = 3 - player.health;

      bullet.kill();
    }, null, this)

    // kill bullets outside game world
    this.physics.arcade.overlap(bullets, function(bullet, bounds) {
      console.log('bullet');
    }, null, this);

    // gun heat
    if (overheat >= 100) {
      overheated = true;
    }

    if (overheat > 0) {
      overheat--;
    } else {
      overheated = false;
    }

    // score display
    scoretext.text = score;

    // gun heat bar
    bars.clear();
    if (overheated) {
      bars.lineStyle(2,0xff0000, 1);
    } else {
      bars.lineStyle(2,0xffffff, 1);
    }

    bars.moveTo(this.game.width - 2 - (overheat * 0.72), 32);
    bars.lineTo(this.game.width - 2, 32);

    bars.drawShape(new Phaser.Line(this.game.width - 2 - (overheat * 0.72), 28, this.game.width - 2, 28));

    // decrease cooldowns
    shotCooldown--;
    spawnCooldown--;

    this.camera.setPosition(player.x, player.y);

    if (player.health <= 0) {
      this.quitGame();
    }
  },

  quitGame: function(pointer) {

    //	Here you should destroy anything you no longer need.
    //	Stop music, delete sprites, purge caches, free resources, all that good stuff.
    groundTiles.destroy();
    player.destroy();
    bullets.destroy();
    enemies.destroy();
    enemybullets.destroy();


    //	Then let's go back to the main menu.
    this.state.start('MainMenu');

  }

};
