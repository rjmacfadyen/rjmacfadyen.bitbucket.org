BasicGame.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;



};

BasicGame.Preloader.prototype = {

	preload: function () {

		//	These are the assets we loaded in Boot.js
		//	A nice sparkly background and a loading progress bar

		this.background = this.add.tileSprite(0, 0, this.game.width, this.game.height, 'background');
		this.preloadBar = this.add.sprite(this.game.width / 2 - 200, this.game.height / 2 - 1, 'preloaderBar');

  	this.backgroundColor = 0xffffff;

		//	This sets the preloadBar sprite as a loader sprite.
		//	What that does is automatically crop the sprite from 0 to full-width
		//	as the files below are loaded in.

		this.load.setPreloadSprite(this.preloadBar);

		// this.load.tilemap('level','assets/level.json',null,Phaser.Tilemap.TILED_JSON);

		// this.load.image('level_tiles','assets/level_bank.png');

		//this.load.spritesheet('player', '/assets/demos/shooter/player.png', 16, 16, 10);
		//	Here we load the rest of the assets our game needs.
		//	You can find all of these assets in the Phaser Examples repository

	    // e.g. this.load.image('image-name', 'assets/sprites/sprite.png');

      this.load.image('player', '/assets/demos/shooter/player.png');
			this.load.image('player-bullet', '/assets/demos/shooter/player-bullet.png');

			this.load.image('enemy', '/assets/demos/shooter/enemy.png');
			this.load.image('enemy-bullet', '/assets/demos/shooter/enemy-bullet.png');

			this.load.spritesheet('lives', '/assets/demos/shooter/lives.png',72,24);

	},

	create: function () {

        console.log("Creating preload state!");

		this.state.start('MainMenu');

	}

};
