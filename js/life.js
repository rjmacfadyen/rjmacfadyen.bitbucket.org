var width = 50;
var height = 50;
var stepSpeed;
var cells = new Array(width * height);

// initCells();
buildGrid();
buildControlPanel();

// add alive to cells when clicked
$('#life td').click(function() {
  if ($(this).hasClass("alive")) {
    $(this).removeClass("alive");
  } else {
    $(this).addClass("alive");
  }
});


// control functions
$('#clear').click(function() {
  $('#life td').removeClass('alive');
})

$('#step').click(function() {
  lifeStep();
})

$('#speed').change(function() {
clearInterval(stepSpeed);

  if ($(this).val() == 0) {
    $('#step').removeAttr('disabled');
  } else {
    stepSpeed = setInterval(lifeStep, (1000 / $(this).val()));
    $('#step').attr('disabled',true);
  }

  $(this).val($(this).val());

})

// handle auto-pause on roll over
$('#lifegrid').mouseenter(function() {
  if ($('#autopause').val()) {
    clearInterval(stepSpeed);
  }
})

$('#lifegrid').mouseleave(function() {
  clearInterval(stepSpeed);
  if ($('#speed').val() > 0) {
    stepSpeed = setInterval(lifeStep, (1000 / $('#speed').val()));
  }
})


function buildGrid() {
  $('#life').append('<table id="lifegrid">');
  for (var y = 0; y < height; y++) {
    $('#lifegrid').append('<tr id="row' + y + '">');
    for (var x = 0; x < width; x++) {
      $('#row' + y).append('<td id="cell' + (y * width + x) + '">');
    }
    $('#lifegrid').append('</tr>');
  }
  $('#life').append('</table>');
}

function buildControlPanel() {
  $('#life').append('<form id="lifecontrols">');
  $('#lifecontrols').append('<input type="button" id="clear" value="Clear">');
  $('#lifecontrols').append('<input type="button" id="step" value="Step">');
  $('#lifecontrols').append('<label for="speed">Speed</label>');
  $('#lifecontrols').append('<input type="range" id="speed" min="0" max="30" value="0" step="1">');
  $('#lifecontrols').append('<label for="autopause">Pause on rollover</label>');
  $('#lifecontrols').append('<input type="checkbox" id="autopause" checked="true">')
}

function lifeStep() {
  for (var i = 0; i < width * height; i++) { // loop through every cell
    var aliveNeighbours // number of neighbouring cells which have alive class

    aliveNeighbours = $('#cell' + neighbour(i,-1,-1)).hasClass('alive') + // add up total of all neighbouring cells
                      $('#cell' + neighbour(i,-1,0)).hasClass('alive') +
                      $('#cell' + neighbour(i,-1,1)).hasClass('alive') +
                      $('#cell' + neighbour(i,0,-1)).hasClass('alive') +
                      $('#cell' + neighbour(i,0,1)).hasClass('alive') +
                      $('#cell' + neighbour(i,1,-1)).hasClass('alive') +
                      $('#cell' + neighbour(i,1,0)).hasClass('alive') +
                      $('#cell' + neighbour(i,1,1)).hasClass('alive');

    if (aliveNeighbours <= 1 || aliveNeighbours >= 4) { // too few or too many neighours, cell dies
      cells[i] = false;
    } else if (aliveNeighbours == 3) { // exactly 3 neighbours, cell is born
      cells[i] = true;
    } else { // cell does not change
      cells[i] = $('#cell' + i).hasClass('alive');
    }
  }

  for (var i = 0; i < width * height; i++) { // loop through table cells and display new cell grid
    if (cells[i]) {
      $('#cell' + i).addClass('alive');
    } else {
      $('#cell' + i).removeClass('alive');
    }
  }
}

function neighbour(cell, xOffset, yOffset) { // helper method for getting index of neighbouring cells
  var cellX = cell % width;
  var cellY = (cell - cellX) / width;

  var newX = (cellX + xOffset + width) % width;
  var newY = (cellY + yOffset + height) % height;

  return (newY * height) + newX;
}
