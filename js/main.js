// expand sidebar on small screens

$('.sidebar-display-expand').click(function() {
  if ($('aside').attr('class') == 'collapsed') {

    // expand sidebar
    $('aside').attr('class', 'expanded');
    // change expand icon
    $('.sidebar-display-expand').attr('src','/assets/images/sidebar-collapse.png')

  } else {

    // collapse sidebar
    $('aside').attr('class', 'collapsed');
    // change expand icon
    $('.sidebar-display-expand').attr('src','/assets/images/sidebar-expand.png')

  }
})
