// resize launch page to fit full viewport height
window.onload = resize;
window.onresize = resize;

function resize() {
  var height = $(window).height() - ($('header').outerHeight() + $('footer').outerHeight()) - 36; // 36 extra pixels from somewhere
  $('main').attr('style','min-height: ' + height + 'px;');
  console.log()
}
