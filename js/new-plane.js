var width = 640;
if ($('#plane').width() < 640) {
  width = $('#plane').width();
}
var scale = width / 640

var game = new Phaser.Game(width,width * 0.75, Phaser.AUTO, 'plane', {preload: preload, create: create, update: update});

var plane;
var coins;
var enemies;
var explosions;
var bullets;

var score = 0;
var scoretext;

var health = 100;

var bars = new Graphics(game, 0, 0);

var spawnCooldown = 100;
var shotCooldown = 20;

var gamePlaying = false;

function preload() {

    game.load.baseURL = 'http://examples.phaser.io/assets/';
    game.load.crossOrigin = 'anonymous';

    game.load.image('sky', 'skies/deepblue.png');
    game.load.image('plane', 'sprites/plane.png');
    game.load.spritesheet('coin', 'sprites/coin.png', 32, 32);
    game.load.image('bullet', 'sprites/bullet.png');
    game.load.spritesheet('explosion', 'sprites/explosion.png', 64, 64);
}

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);


    game.add.sprite(0,0,'sky');


    plane = game.add.sprite(s(100), game.world.height / 2, 'plane');
    game.physics.arcade.enable(plane);
    plane.body.gravity.y = 0;
    plane.scale.setTo(s(1),s(1));
    plane.pivot.x = s(10);
    plane.pivot.y = s(10);


    coins = game.add.group();
    coins.enableBody = true;
    scoretext = game.add.text(16,16,'0',{fontsize: s(32) + 'px', fill: '#fff'});

    explosions = game.add.group()

    bullets = game.add.group();
    bullets.enableBody = true;
}

function update() {
  if (gamePlaying) {
    gameTick();
  } else {
    menuScreen();
  }

  coins.forEach(function(item) { // move coins across the screen
      item.x -= 1;
  }, this);

  explosions.forEach(function(item) { // kill explosions which have finished playing
      if (item.animations.frame == 22) {
        item.kill();
      }
  })
}

function startGame() {
  plane.x = s(100);
  plane.y = game.world.height / 2;
  plane.body.gravity.y = s(300);

  plane.visible = true;

  score = 0;
  scoretext.text = 0;

  gamePlaying = true;
}

function gameTick() {

    cursors = game.input.keyboard.createCursorKeys();

    if (cursors.up.isDown) {
        plane.body.velocity.y -= s(10);
    }

    plane.rotation = plane.body.velocity.y / s(500);

    // collect coins
    game.physics.arcade.overlap(plane,coins,collectCoin,null,this);
    // shoot coins
    game.physics.arcade.overlap(bullets,coins,shootCoin,null,this);

    spawnCooldown--;
    shotCooldown--;

    // spawn coins
    if (spawnCooldown <= 0) {
        coin = coins.create(game.world.width, Math.random() * game.world.height, 'coin');
        coin.animations.add('spin', [0,1,2,3,4,5], 10, true);
        coin.scale.setTo(s(0.5),s(0.5));
        coin.animations.play('spin');

        spawnCooldown = 100 + Math.random() * 200;
    }

    // shoot bullets
    if (shotCooldown <= 0 && cursors.down.isDown) {
      bullet = bullets.create(plane.x, plane.y, 'bullet');
      bullet.body.velocity.x = s(Math.cos(plane.rotation) * 200);
      bullet.body.velocity.y = s(Math.sin(plane.rotation) * 200);
      bullet.rotation = plane.rotation + Math.PI / 2;
      bullet.scale.setTo(s(1),s(1));

      shotCooldown = 10;
    }

    if (plane.y <= 0 || plane.y >= game.world.height) {
      gameOver();
    }

}

function menuScreen() {
  cursors = game.input.keyboard.createCursorKeys();

  scoretext.text = 'Press up to start';
    if (cursors.up.isDown) {
      startGame();
    }
}

function collectCoin(player, coin) {
    coin.kill();
    score++;
    scoretext.text = score;
}

function shootCoin(bullet, coin) {
    explode(coin, 0.5);
    coin.kill();
}

function gameOver() {
  explode(plane, 1);
  plane.visible = false;
  plane.y = game.world.height / 2;
  plane.body.gravity.y = 0;
  plane.body.velocity.y = 0;

  gamePlaying = false;

}

function explode(explodingSprite, size) {
  explosion = explosions.create(explodingSprite.x - s(32 * size), explodingSprite.y - s(32 * size), 'explosion');
  explosion.animations.add('explode', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 10, false);
  explosion.scale.setTo(s(size),s(size));
  explosion.animations.play('explode');
  // explosion.events.onAnimationComplete.add(function() {this.destroy()}, this);
}

function s(num) { // quick function for scaling the game
  return num * scale
}
